import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Compo1Component } from './components/compo1/compo1.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { SobreComponent } from './components/sobre/sobre.component';
import { BiometriaComponent } from './components/estudios/biometria/biometria.component';
import { CovidComponent } from './components/estudios/covid/covid.component';
import { EsesComponent } from './components/estudios/eses/eses.component';
import { OrinaComponent } from './components/estudios/orina/orina.component';
import { QuimicaComponent } from './components/estudios/quimica/quimica.component';

const routes: Routes = [
  {
    path:'', 
    component:Compo1Component  //definimos el componente1 como componente principal
  },
  {
    path:'contacto', 
    component:ContactoComponent  
  },
  {
    path:'sobre', 
    component:SobreComponent  
  },
  {
    path:'biometria', 
    component:BiometriaComponent  
  },
  {
    path:'covid', 
    component:CovidComponent  
  },
  {
    path:'eses', 
    component:EsesComponent
  },
  {
    path:'orina', 
    component:OrinaComponent  
  },
  {
    path:'quimica', 
    component:QuimicaComponent  
  },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
