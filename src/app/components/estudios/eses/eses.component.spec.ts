import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EsesComponent } from './eses.component';

describe('EsesComponent', () => {
  let component: EsesComponent;
  let fixture: ComponentFixture<EsesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EsesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EsesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
