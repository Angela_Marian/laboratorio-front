import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Compo1Component } from './components/compo1/compo1.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { SobreComponent } from './components/sobre/sobre.component';
import { QuimicaComponent } from './components/estudios/quimica/quimica.component';
import { EsesComponent } from './components/estudios/eses/eses.component';
import { OrinaComponent } from './components/estudios/orina/orina.component';
import { CovidComponent } from './components/estudios/covid/covid.component';
import { BiometriaComponent } from './components/estudios/biometria/biometria.component';

@NgModule({
  declarations: [
    AppComponent,
    Compo1Component,
    ContactoComponent,
    SobreComponent,
    QuimicaComponent,
    EsesComponent,
    OrinaComponent,
    CovidComponent,
    BiometriaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
